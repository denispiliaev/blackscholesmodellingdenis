from math import sqrt, log, exp, erf
import random
from numpy import arange
import matplotlib.pyplot as plt
import pandas as pd
import os
import math

import numpy as np
import scipy.stats as si
import sympy as sy


###Black-Sholes

S0 = 100.0  # S0 = Stock price
strikes = [i for i in range(50, 150)]  # Exercise prices range   #strikes
T = 1  # T = Time to expiration
r = 0.01  # r = risk-free interest rate
q = 0.02  # q = dividend yield
vol = 0.2  # vol = volatility

def bs_call (S0, strikes, T, r, q, vol) :
    d1 = (np.log(S0 / strikes) + (r - q + 0.5 * vol ** 2) * T) / (vol * np.sqrt(T))
    d2 = (np.log(S0 / strikes) + (r - q - 0.5 * vol ** 2) * T) / (vol * np.sqrt(T))

    callprice = (S0 * si.norm.cdf(d1, 0.0, 1.0) - strikes * np.exp(-r * T) * si.norm.cdf(d2, 0.0, 1.0))

    return callprice

bs_call(100.0, 50, 1, 0.01, 0.02, 0.02)

import pandas as pd
import numpy as np

df = pd.DataFrame(np.random.randint(0, 100, size =(500, 4)),
                  columns=list('ABCD'))

print(df)

df.loc[df['A'] > 50, 'A'] = 75
df.loc[df['B'] > 50, 'B'] = 80
df.loc[df['C'] > 50, 'C'] = 85 # contains a bug - not anymore
df.loc[df['D'] > 50, 'D'] = 90

print(df)


